module.exports = {
  mode: 'jit',
  theme: {
    extend: {
      fontFamily: {
        lato: ['"Lato"', 'ui-sans-serif', 'system-ui', '-apple-system'],
      },
      colors: {
        primary: '#319795',
        secondary: '#3182CE',
      },
      boxShadow: {
        top: '0px -1px 3px #00000033',
      },
    },
  },
  variants: {
    opacity: ({ after }) => after(['disabled']),
    cursor: ({ after }) => after(['disabled']),
    backgroundImage: ['hover'],
    background: ['hover'],
  },
  plugins: [],
  purge: {
    enabled: process.env.NODE_ENV === 'production',
    content: ['components/**/*.vue', 'layouts/**/*.vue', 'pages/**/*.vue'],
  },
}
